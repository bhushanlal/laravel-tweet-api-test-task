<?php
namespace App\Traits;

use Illuminate\Support\Facades\Validator;

trait ValidationTrait
{
    /**
     * @Validaiton   : Login Rules
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Array of response data.
    */
    public $loginRules = [
        'email' => 'required|email',
        'password' => 'required'
    ];

    /**
     * @Validaiton   : Tweet Saving Rules
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Array of response data.
    */
    public $tweetSaveRules = [
        'tweet' => 'required|max:140'
    ];

    /**
     * @Validaiton   : Fetch User Tweet Rules
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Array of response data.
    */
    public $userTweetRules = [
        'user_id' => 'required|integer'
    ];

    /**
     * @Validaiton   : Update User Profile Rules
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Array of response data.
    */
    public $updateProfileRules = [
        'name' => 'required|regex:/(^[A-Za-z ]+$)+/',
        'image' => 'file',
        'dob' => 'required|date'

    ];

    /**
     * @Purpose      : Common function to validate request data.
     * @params       : Request Data & Rules
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       :Validation Response
    */
    public function validateRequest($requestData,$rules){
        
        $validator = Validator::make($requestData, $rules);  
        if ($validator->fails()) {
            return ['message' => $validator->errors()->first(), 'status' => 'failed', 'error_code'=>422];
        }
        return ['message' => 'Validation Passes.', 'status' => 'success'];
    }
}
