<?php

namespace App\Http\Controllers;

use App\Models\UserProfile;
use App\Traits\ValidationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 

class ProfileController extends Controller
{
    use ValidationTrait;
    public $userId;

    public function __construct()
    {
        $this->userId = Auth::id();
    }

    /**
     * @method       : GET
     * @params       : N/A
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Return Profile Data.
    */
    public function view(){
        try {
            //Fetching profile data.
            $records = UserProfile::whereUserId($this->userId)->get();
            if($records->isEmpty()){
                return response()->json(['message' => 'No record found.'],404);
            }
            //Returning user's profile data.
            return response()->json(['message' => 'User Profile' ,'data' => $records->toArray()]);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }

    /**
     * @method       : POST
     * @params       : name, image and dob.
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Return success/error message.
    */
    public function edit(Request $request){
        try {
            //Validating Request Data
            $validation = $this->validateRequest($request->all(),$this->updateProfileRules);
            if($validation['status'] == 'failed'){
                return response()->json($validation,$validation['error_code']);
            }
            //Creating data array for updating user profile.
            $data = [
                "name" => $request->name,
                "dob" => $request->dob
            ];
            //Saving new image.
            if($request->has('image') && !empty($request->image)){
                $image = $request->file('image');
                $image_name = 'image'.$this->userId.time().'.'.$image->getClientOriginalExtension();
                $image->storeAs('public/user_images', $image_name); 
                $data['image'] = $image_name;
                //Deleting old image from strorage folder.
                $old_image = auth()->user()->userProfile->image;
                $old_image = str_replace(url('storage/').'/user_images/','',$old_image);
                File::delete(storage_path().'/app/public/user_images/'.$old_image);
            }
            //Updating user's profile
            UserProfile::whereUserId($this->userId)->update($data);
            return response()->json(['message' => 'Profile updated successfully.']);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }
}
