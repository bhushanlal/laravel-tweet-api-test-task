<?php

namespace App\Http\Controllers;

use App\Traits\ValidationTrait;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ValidationTrait;
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * @method       : POST
     * @params       : email & password
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Access Token (Type: Bearer)
    */
    public function login(Request $request)
    {
        try {
            //Validating Request Data
            $validation = $this->validateRequest($request->all(),$this->loginRules);
            if($validation['status'] == 'failed'){
                return response()->json($validation,$validation['error_code']);
            }
            //Checking User Credentials.
            $credentials = request(['email', 'password']);
            if (! $token = auth()->attempt($credentials)) {
                return response()->json(['message' => 'Invalid username or password'], 401);
            }
            return response()->json(['message' => 'Account logged-in successfully.', 'bearer_token' => $token]);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }

    /**
     * @method      : POST
     * @params      : N/A
     * @created_date: 10-12-2021 (dd-mm-yyyy)
     * @return      : JSON response message.
    */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out.']);
    }
}
