<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use App\Traits\ValidationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TweetController extends Controller
{
    use ValidationTrait;
    public $userId;

    public function __construct()
    {
        $this->userId = Auth::id();
    }

    /**
     * @method       : POST
     * @params       : tweet
     * @created_date : 10-12-2021 (dd-mm-yyyy)
     * @return       : Return success/error message.
    */
    public function postTweet(Request $request){
        try {
            //Validating Request Data
            $validation = $this->validateRequest($request->all(),$this->tweetSaveRules);
            if($validation['status'] == 'failed'){
                return response()->json($validation,$validation['error_code']);
            }
            //Adding new key value pairs in request data.
            $request->request->add(['user_id' => $this->userId]);
            //Saving user's tweet.
            Tweet::create($request->all());
            return response()->json(['message'=>'Tweet posted successfully.']);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }

    /**
     * @method      : GET
     * @created_date: 10-12-2021 (dd-mm-yyyy)
     * @return      : Return user's tweets. 
    */
    public function getUserTweets(Request $request){
        try {
            //Validating Request Data
            $validation = $this->validateRequest($request->all(),$this->userTweetRules);
            if($validation['status'] == 'failed'){
                return response()->json($validation,$validation['error_code']);
            }
            //Fetching user tweets.
            $records = Tweet::with(['userProfile' => function($query){
                $query->select('user_profiles.id', 'user_id', 'name', 'image');
            }])->whereUserId($request->user_id)->paginate(5);
            if($records->isEmpty()){
                return response()->json(['message' => 'No tweet found.'],404);
            }
            return response()->json(['message' => 'User Tweets' ,'data' => $records->toArray()]);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }

    /**
     * @method      : GET
     * @created_date: 10-12-2021 (dd-mm-yyyy)
     * @return      : Return random tweets.
    */
    public function getAllTweets(){
        try {
            //Fetching user tweets.
            $records = Tweet::inRandomOrder()->with(['userProfile' => function($query){
                $query->select('user_profiles.id', 'user_id', 'name', 'image');
            }])->paginate(5);
            if($records->isEmpty()){
                return response()->json(['message' => 'No tweet found.'],404);
            }
            return response()->json(['message' => 'User Tweets' ,'data' => $records->toArray()]);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }
}
