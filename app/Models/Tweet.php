<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'tweet'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * Tweet model relation with user profile model through user model.
    */
    public function userProfile()
    {
        return $this->hasOneThrough(UserProfile::class, User::class,'id','user_id','user_id','id');
    }

    /**
     * Tweet model relation with user model.
    */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
