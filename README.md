Laravel Backend Test Task
# Setup Instructions

Copy .env from .env.example file.

Create a new DB.

Change the DB credentials from the .env file.

Run "Composer install" to setup the dependecies.

Run the following artisan commands.

php artisan migrate --seed

php artisan jwt:secret

php artisan storage:link

php artisan serve

Use the below credentials for the login API

email:�alex@yopmail.com

password: 123456789

# API

api/login, Login api to authenticate the user.

api/post-tweet, Api to create the Tweet.

api/get-user-tweets, Api to get the tweets by the user_id with pagination.

api/get-all-tweets, Api to get random tweets with pagination.

api/view-profile, APi to view the loggedIn user profile data.

api/update-profile, Api to update the profile data.

api/logout, Api to logout the user.