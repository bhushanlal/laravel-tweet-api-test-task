<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
            "email" => 'alex@yopmail.com',  
            "password" => Hash::make('123456789'),
            "userProfile" => [
                    "name" => "Alex",
                    "image" => "alex.png",
                    "dob" => "1996-12-10"
                ]
            ],
            [
            "email" => 'peter@yopmail.com',
            "password" => Hash::make('123456789'),  
            "userProfile" => [
                    "name" => "Peter",
                    "image" => "peter.png",
                    "dob" => "1996-12-10"
                ]
            ],
            [
            "email" => "sam@yopmail.com",
            "password" => Hash::make('123456789'), 
            "userProfile" => [
                    "name" => "Sam",
                    "image" => "sam.png",
                    "dob" => "1996-12-10"
                ] 
            ],
            [
            "email" => "john@yopmail.com",
            "password" => Hash::make('123456789'),  
            "userProfile" => [
                    "name" => "John",
                    "image" => "john.png",
                    "dob" => "1996-12-10"
                ]
            ]
        ];
        foreach ($records as $record) {
            $userData = Arr::except($record,['userProfile']);
            $user = User::create($userData);
            $userProfileData = $record['userProfile'];
            $userProfileData['user_id'] = $user->id;
            $user = UserProfile::create($userProfileData);
        } 
    }
}
