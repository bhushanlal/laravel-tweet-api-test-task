<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//User Login
Route::post('/login', 'AuthController@login');
//Get user specific tweets.
Route::get('/get-user-tweets', 'TweetController@getUserTweets');
//Get random tweets.
Route::get('/get-all-tweets', 'TweetController@getAllTweets');

//Authenticated Routes
Route::group(['middleware' => 'auth'], function ($router) {
    //Post user tweet.
    Route::post('/post-tweet', 'TweetController@postTweet');
    //View logged-in user profile.
    Route::get('/view-profile', 'ProfileController@view');
    //Update logged-in user profile.
    Route::post('/update-profile', 'ProfileController@edit');
    //Logout
    Route::get('/logout', 'AuthController@logout');
});